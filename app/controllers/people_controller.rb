class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]


  helper_method :home
  def home
       @people = Person.all
  end

  helper_method :fact
  def fact
       @people = Person.all
  end     
  
  helper_method :feed
  def feed
       @people = Person.all
  end     


  # all entries (Person model) inside database called people
  def index
    @people = Person.all
  end



  # view one particular entry (Person model); recorded by id
  def show
  end



  # create new entry (Person model)
  def new
    @person = Person.new
  end



  # what we actually define when we -> create new entry (Person model)
  def create
    @person = Person.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end



  # edit entry (Person model)
  def edit
  end



  # what we actually define when we -> edit entry (Person model)
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end



  # remove an entry (Person model)
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:name, :weight, :height, :color, :handedness, :writeStyle, :writeFont, :writeColor)
    end
end
