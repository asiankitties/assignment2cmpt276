json.array!(@people) do |person|
  json.extract! person, :id, :name, :weight, :height, :color, :handedness, :writeStyle, :writeFont, :writeColor
  json.url person_url(person, format: :json)
end
