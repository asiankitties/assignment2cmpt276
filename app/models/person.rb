class Person < ActiveRecord::Base

validates :name, presence: true
validates :weight, presence: true

validates :height, presence: true
validates :color, presence: true

validates :handedness, presence: true
validates :writeStyle, presence: true

validates :writeFont, presence: true
validates :writeColor, presence: true


end
