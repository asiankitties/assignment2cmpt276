class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.integer :weight
      t.integer :height
      t.string :color
      t.string :handedness
      t.string :writeStyle
      t.string :writeFont
      t.string :writeColor

      t.timestamps null: false
    end
  end
end
